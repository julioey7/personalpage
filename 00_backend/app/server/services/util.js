const moment = require('moment');
const _ = require('lodash');

async function asyncEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

function getCleanToken(token) {
    if(!token) return null;
    return token.replace('Bearer ', '') || null;
}

function wrapAsync(fn) {
    return function(req, res, next) {
        // Make sure to `.catch()` any errors and pass them along to the `next()`
        // middleware in the chain, in this case the error handler.
        fn(req, res, next).catch(next);
    };
}

function getStartDate(games) {
    let startDate = moment('2050-01-01');
    let reference = moment('2019-01-01');
    let date;

    _.each(games, game => {
        date = moment(game.date);
        if(date.isSameOrAfter(reference) && date.isBefore(startDate)) {
            startDate = date
        }
    });

    return startDate;
}

module.exports = { asyncEach, getCleanToken, wrapAsync, getStartDate };