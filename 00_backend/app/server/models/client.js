
// USER MODEL
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let client = new Schema({
    name: {
        type: String,
        required: [true, 'com.app.client.error.name.required'],
    },
    email: {
        type: String,
        lowercase: true,
        required: [true, 'com.app.client.error.email.required'],
        match: [/\S+@\S+\.\S+/, 'com.app.client.error.email.invalid'],
        unique: true,
        index: true
    },
    phone: {
        type: Number,
        required: false
    },
    subject: {
        type: String,
        required: false
    }
}, {timestamps: true});

client.plugin(uniqueValidator, { message: 'com.app.client.error.{PATH}.duplicated' });
client.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
};

client.pre('save', async function(next) {
    if(this.isModified('password') || this.isNew) {
        this.password = await argon2.hash(this.password);
        return next();
    }

    return next();
});

client.methods.validatePassword = async function(password) {
    return await argon2.verify(this.password, password)
};

module.exports = mongoose.model('User', client);