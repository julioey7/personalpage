const express = require('express');
const router = express.Router();
const util = require('../services/util');

const indexController = require('../controllers/index');

router.post('/contact', util.wrapAsync(indexController.contact));

module.exports = router;