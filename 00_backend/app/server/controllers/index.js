const Client = require('../models/client');
const mailService = require('../services/mail');

module.exports = class indexController {

    static async contact(request, response) {
        let json = request.body;
        if (!json.name || !json.email || !json.phone || !json.subject) {
            let field = !json.name ? 'name' : !json.email ? 'email' : !json.phone? 'phone' : 'subject';
            response.status(409).send(`com.app.request.error.${field}.required`);
            return
        }

        let client = await Client.find({ email: json.email });
        if(!client) client = new Client();

        client.name = json.name;
        client.email = json.email;
        client.phone = json.phone;
        client.subject = json.subject;

        await client.save();

        let params = {
            user: client,
            type: 'contact',
            message: json.message,
            locale: request.headers['accept-language'] || 'es_MX'
        };

        await mailService.sendEmail(params);
        response.status(200).send('com.app.request.success.contact.mail.sent');
    }

};