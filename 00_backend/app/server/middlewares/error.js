

function basicErrorHandler(error, request, response, next) {
    let status = error.name === 'ValidationError'? 409 : 500;
    console.info("Sending error to client");
    response.status(status).send(error.message);
}

module.exports = {basicErrorHandler};