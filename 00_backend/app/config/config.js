
// GLOBAL CONFIGURATIONS


// PORT
process.env.PORT = process.env.PORT || 3000;


// ENVIRONMENT
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// TIMEZONE
process.env.TZ = 'UTC-6';

// DATABASE
process.env.DBURL = process.env.DBURL || 'mongodb://personaladmin:personaley7.@ds161038.mlab.com:61038/personalpage';